import Vue from 'vue/dist/vue.js';
import * as components from './components';
import SampleList from './data/SampleList.json';

/**
 * Application setup
 *
 * @class App
 */
export default class App {
    constructor() {
        this.appName = 'vueTutorial';

        this.defineComps(components);

        this.instance = new Vue({
            el: `[data-app="${this.appName}"]`,
            data: {
                listName: null,
                todoList: [],
            },
            methods: {
                loadSampleList() {
                    this.mapRawToUi(SampleList.SampleTodos);
                },
                mapRawToUi(rawData) {
                    const rawList = rawData.list;

                    rawList.forEach((indvItem, idx) => {
                        this.todoList.push(Object.assign({}, indvItem));
                    });

                    this.listName = rawData.name;
                },
                addNewTodo(evtPkg) {
                    console.log('addNewTodo - evtPkg: ', evtPkg);
                    if (evtPkg == null) {
                        return false;
                    }
                    const todoObj = {
                        name: evtPkg,
                        complete: false,
                    };

                    this.todoList.push(Object.assign({}, todoObj));
                },
            },
        });
    }

    defineComps(compsObj) {
        const compNames = Object.keys(compsObj);

        compNames.forEach(indvName => {
            Vue.component(indvName, compsObj[indvName]);
        });
    }
}
