/**
 * Builds CSS files found in /src/assets/styles
 *
 * @usage gulp styles
 */

import browserSync from 'browser-sync';
import cleanCSS from 'gulp-clean-css';
import gulp from 'gulp';
import gulpIf from 'gulp-if';
import sourcemaps from 'gulp-sourcemaps';
import notify from './notify';
// SASS DEPS
import sass from 'gulp-sass';

const FILE_EXT = '*.scss';
let DIR_DEST = `${process.env.DIRECTORY_DEST}/assets/styles`;
let DIR_SRC = `${process.env.DIRECTORY_SRC}/assets/styles`;

if (process.env.DIRECTORY_STYLES_DEST != null
    && process.env.DIRECTORY_STYLES_DEST !== 'null'
) {
    DIR_DEST = process.env.DIRECTORY_STYLES_DEST;
}

if (process.env.DIRECTORY_STYLES_SRC != null
    && process.env.DIRECTORY_STYLES_SRC !== 'null'
) {
    DIR_SRC = process.env.DIRECTORY_STYLES_SRC;
}

function watchStyles() {
    gulp.watch(`${DIR_SRC}/**/*`, () => {
        notify.log('STYLES: file update detected, rebuilding...');
        buildStyles();
    });
}

function buildStyles() {
    const browser = browserSync.get('local');

    return gulp
        .src(`${DIR_SRC}/${FILE_EXT}`)
        .pipe(notify.onError('STYLES: error'))
        .pipe(gulpIf(process.env.SOURCE_MAPS === 'true', sourcemaps.init()))
        // .pipe(postcss(processors))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulpIf(process.env.MINIFY === 'true', cleanCSS()))
        .pipe(gulpIf(process.env.SOURCE_MAPS === 'true', sourcemaps.write('./')))
        .pipe(gulp.dest(`${DIR_DEST}/`))
        .on('end', notify.onLog('STYLES: rebuild complete'))
        .pipe(browser.reload({ match: `**/${FILE_EXT}`, stream: true }));
}

export default function styles() {
    if (process.env.WATCH === 'true') {
        watchStyles();
    }

    return buildStyles();
}
