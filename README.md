# VUE.Tutorial

This is a beginners guide to Vue.js and structuring Vue to work with the Nerdery's FE Boilerplate. This includes the setup of Vue.js as part of the Nerdery Front-End boilerplate. All steps will be broken out into separate branches so that they may be looked at individually. Each branch will be listed with the ste in this README document.

*Dependencies:*

 * [vue](https://www.npmjs.com/package/vue)
 * [vueify](https://www.npmjs.com/package/vueify)
 * [stringify](https://www.npmjs.com/package/stringify)
 * [gulp-sass](https://www.npmjs.com/package/gulp-sass) (optional)

*Helpful Links:*

 * [Vue Templates](https://github.com/vuejs-templates)
 * [Vue & Browserfy](https://github.com/vuejs-templates/browserify)
 * [Vue Components](https://vuejs.org/v2/guide/components.html#What-are-Components)
 * [7 Ways to Define a Component Template](https://vuejsdevelopers.com/2017/03/24/vue-js-component-templates/)

## Nerdery FE Boilerplate Setup Instructions

See README-BOILERPLATE.md for information regarding the Nerdey's FE Boilerplate setup.

 1. run `npm install`

## STEP: Setup of SASS Instead of POST CSS

Branch: `feature/build-with-sass`

This step is for setting up SASS instead of POST CSS for the styles build task. This is not necessary for the Vue.js portion of the tutorial so you may skip past this step.

### Install Gulp SASS

Run fromm terminal:
`npm install --save-dev gulp-sass`

### Remove POST CSS Dependencies

File(s):

 * `./tasks/styles.js`

Remove the following dependencies:

```
import discardComments from 'postcss-discard-comments';
import postcss from 'gulp-postcss';
import cssnext from 'postcss-cssnext';
import atImport from 'postcss-import';
import apply from 'postcss-apply';
```

Remove the following POST CSS Configuration Options:

```
const processors = [
    atImport,
    discardComments,
    apply,
    cssnext,
];
```

### Add gulp-sass Dependency

File(s):

 * `./tasks/styles.js`

Add dependency to the list of dependencies:

`import sass from 'gulp-sass';`

Find the folowing postcss task:

`.pipe(postcss(processors))`

Replace with sass task:

`.pipe(sass().on('error', sass.logError))`

Replace all `*.css` extensions with `*.scss` extensions.

## SETUP: Integrate Vue.js as Part of Nerdery FE Boilerplate

Branch: `feature/build-with-vue`

Before we get started you may want to download the [Vue Developer Tools](https://github.com/vuejs/vue-devtools). If you are working in chrome you can download the tools as a chrome extension or you can do a manual setup.

*Vue Developer Tools*

 * [Repository](https://github.com/vuejs/vue-devtools)
 * [Chrome Extension](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)

In this initial setup we will be going into the Nerdery's FE Boilerplate and setting Vue.js up to work with the rest of the build tools that are already available. Because the Nerdery Front-End build leverages `browserify` we will be using `vueify` to build Vue.js application.

### Install Vue and Vueify

Run from terminal:

 * `npm install --save vue`
 * `npm install --save-dev vueify`

After this run `npm install` from your terminal if you have not already done so.

### Add Vueify Dependency and Integrate with Scripts Task

Add vueify dependency.

`import vueify from 'vueify';`

Add browserify transform for vueify to the bundler in `function buildScripts() { ...` after the browserify transformation.

```
const bundler = browserify({
        ...
    })
    ...
    .transform('babelify', { extensions: ['.js'] })
    .transform('vueify');
```

### Make a Simple Application to Test Vue.js Build

File(s):

 * `./src/scripts/App.js` (application kickoff)
 * `./src/index.hbs` (markup)

In the `./src/scripts/App.js` file add the `vue` dependency.

`import Vue from 'vue/dist/vue.js';`

Then instanciate the Vue application using the following code. We will be using a data attribute of *data-app*. The value for the data attribute will be the name of the application. To identifiy the kick off we will be using *"vueTutorial"* as the attribute value and application name. In the constructor for the `App` class define the name of the application `this.appName = 'vueTutorial'`. From the class constructor instantiate the Vue application using the data attribute and application name that we setup previously.

In Application kickoff:

```
this.instance = new Vue({
    el: `[data-app="${this.appName}"]`,
    data: {
        message: 'Hello Vue!',
    },
});
```

In Markup:

replace

```
<div class="js-welcome">Loading...</div>
```

with

```
<div data-app="vueTutorial">
    <span v-bind:title="message">
        Vue Tutorial: message data can be seen when hovering over this text.
    </span>
</div>
```

## STEP 1: Introduce Components

Branch: `tutorial/step1-introduce-components`

From Vue.js:

Components are one of the most powerful features of Vue. They help you extend basic HTML elements to encapsulate reusable code. At a high level, components are custom elements that Vue’s compiler attaches behavior to. In some cases, they may also appear as a native HTML element extended with the special `is` attribute.

All Vue components are also Vue instances, and so accept the same options object (except for a few root-specific options) and provide the same lifecycle hooks.

### Global Components

We are going two different ways of creating components. The first will be a simple *Hello World* component that will be defined as a global component.

In `./src/assets/scripts/App.js` we will create the simple *Hello World* component. We will use `Vue.component()` to define our global componet in the constructor of the *App* class just before the instantiation of the Vue instance (`this.instance = new Vue({...`).

*Define Hello World Component:*

```
Vue.component('helloWorld', {
    data() {
        return {
            title: 'Hello World!',
        };
    },
    template: `<div>
                    <h2>{{ title }}</h2>
                </div>`,
});
```

The `data` property is defined as a function instead of a static object like we did for the application. This is because there could be multiple instances of the *Vue* component where as we only will have one instance of the application. The component is defining the template using a string but there are other ways that will make our application more flexible and easier to manage. Things like *x-templtes*, *JSX*, or *Single File Components* are other ways in which Vue.js alows us to define our component templates.

In the `./src/index.hbs` file we are going to try using our newly defined *Hello World* component.

replace

```
<div data-app="vueTutorial">
    <span v-bind:title="message">
        Vue Tutorial: message data can be seen when hovering over this text.
    </span>
</div>
```

with

```
<div data-app="vueTutorial">
    <hello-world></hello-world>
</div>
```

### Streamlining Global Component Creation

File(s):

 * `./src/assets/scripts/App.js`
 * `./src/assets/scripts/components/HelloWorld.js`
 * `./src/assets/scripts/components/index.js`

In this section we will be creating a system that will allow us to create multiple components and a consistant and reliabe manner. Start by creating the `./src/assets/scripts/components` directory. It is in this directory that we will place all of our future components. Now we will begin by extracting the *Hello World* component into its own file.

Create new file `./src/assets/scripts/components/HelloWorld.js` for our *Hello World* component code.

```
import Vue from 'vue/dist/vue.js';

const HellowWorld = {
    data() {
        return {
            title: 'Hello World!!!',
        };
    },
    props: ['subTitle'],
    template: `<div>
                    <h2>{{ title }}</h2>
                    <h3 v-if="subTitle != null">{{ subTitle }}</h3>
                </div>`,
};

export default HellowWorld;
```

Remove Hello World code from `App` class.

```
Vue.component('helloWorld', {
    data() {
        return {
            title: 'Hello World!',
        };
    },
    template: `<div>
                    <h2>{{ title }}</h2>
                </div>`,
});
```

We will not be defining the component with `Vue.component()` from this JS module file. Instead we will be creating `./src/assets/scripts/components/index.js` where we will have an `export` written for each of the components that we create. The `index.js` file can then be imported into the `App.js` file as an object which we will use to define all of the components that we create.

Create `export` for *Hello World* component file in `./src/assets/scripts/components/index.js`:

```
export { default as helloWorld } from './HelloWorld';
```

For the name defining what it is exported `as` use the name of the component element that we'll use in the markup.

*Define All Components for Vue.js*

Import components into `App.js`

```
import * as components from './components';
```

Creat new method on the `App` class for defining the Vue components.

```
defineComps(compsObj) {
    const compNames = Object.keys(compsObj);

    compNames.forEach(indvName => {
        Vue.component(indvName, compsObj[indvName]);
    });
}
```

Where the *Hello World* component code was in the `App` class costructor call to the new method and pass it the imported components.

```
this.defineComps(components);

this.instance = new Vue({
    ...
```

### Use HTML file to Define Component Template

File(s):

 * `./src/assets/scripts/components/template-list-title.html`
 * `./src/assets/scripts/components/ListTitle.js`
 * `./src/assets/scripts/components/index.js`
 * `./src/index.hbs`

Install HTML build transformation for requiring HTML files.

`npm install --save-dev stringify`

Import `stringify` into `./tasks/scripts.js`.

`import stringify from 'stringify';`

Add transform for `stringify` to `function buildSripts() {..` in the `./tasks/scripts.js` file.

```
...
    .transform('stringify', {
        appliesTo: { includeExtensions: ['.html'] },
    })
    .transform('babelify', { extensions: ['.js'] })
    .transform('vueify');
```

For the component template markup create `./src/assets/scripts/components/template-list-title.html` with the following markup.

```
<div class="vr vr_2x">
    <h1 class="hdg hdg_1">{{ title }}</h1>
</div>
```

Now we can create a new component with `./src/assets/components/ListTitle.js`.

```
import Vue from 'vue/dist/vue.js';
import listTitleTemp from './template-list-title.html';

const ListTitle = {
    data() {
        return {
            title: 'Todos',
        };
    },
    template: listTitleTemp,
};

export default ListTitle;
```

Create `export` for the new *List Title* component.

`export { default as listTitle } from './ListTitle';`

In `./src/index.hbs` replace the `<hello-world></hello-world>` component with the new `<list-title></list-title>` component.

### Use Single File Components to Define a Component Template

File(s):

 * `./src/assets/scripts/components/ItemInput.vue`
 * `./src/index.hbs`

Vue.js allows you to create a complete compnent template and all as a single file.

Create a new component file for an *Item Input* component as a `.vue` file (`./src/assets/scripts/components/ItemInput.vue`).

```
<template>
<div class="blocks blocks_3up mix-blocks_alnCenter">
    <div>
        <div class="panel">
            <input name="new item"
                placeholder="New Item"
                type="text"
                v-model.trim="newItem"
                v-on:keyup.enter="addItemToList()" />
        </div>
    </div>
</div>
</template>

<script>
export default {
    data() {
        return {
            newItem: null,
        };
    },
    methods: {
        addItemToList() {
            console.log(`Add, ${this.newItem}, to the item list.`);
        }
    },
};
</script>
```

We are using a couple of new features here with the `v-` attributes on the input and the `methods` prop on the component. In Vue the `v-` attributes denote a directive to  be used on various elements. The `methods` prop on the component is just as it appears and obect of methods that can be used to define functions to be used as part of your component. These methods are scope the the Vue component.

Add `export` for the new *Item Input* component to the `./src/assets/components/index.js` file.

Then to see the new item shown on the page place the new component `<item-input></item-input>` in the application markup (`./src/index.hbs`).

## STEP 2: Directives, Lists, and Events

Branch: `tutorial/step2-directives-lists-events`

File(s):

 * `./src/assets/scripts/components/template-todo-list.html`
 * `./src/assets/scripts/components/TodoList.js`
 * `./src/assets/scripts/components/index.js`
 * `./src/assets/scripts/App.js`
