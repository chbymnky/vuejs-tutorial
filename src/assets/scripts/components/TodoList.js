import Vue from 'vue/dist/vue.js';
import todoListTemp from './template-todo-list.html';

const TodoList = {
    data() {
        return {
            title: 'Todos',
        };
    },
    props: ['masterList'],
    template: todoListTemp,
    methods: {
        loadSampleList() {
            this.$emit('sample');
        },
    },
    created() {
        console.log('TodoList - mastList: ', this.masterList);
    },
};

export default TodoList;
