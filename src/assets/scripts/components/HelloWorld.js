import Vue from 'vue/dist/vue.js';

const HellowWorld = {
    data() {
        return {
            title: 'Hello World!!!',
        };
    },
    template: `<div>
                    <h2>{{ title }}</h2>
                </div>`,
};

export default HellowWorld;
