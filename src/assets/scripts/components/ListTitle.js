import Vue from 'vue/dist/vue.js';
import listTitleTemp from './template-list-title.html';

const ListTitle = {
    data() {
        return {
            title: 'Todos',
        };
    },
    template: listTitleTemp,
};

export default ListTitle;
